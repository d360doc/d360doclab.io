# Abril 2017 (Versão 0.4.0)
 
Bem-vindo à versão de número 0.4.0, visamos melhorar a experiencia de centro de custo e tivemos alguns ajustes em compromisso financeiro.

## Centro de Custo 

Os Centros de custos são unidades dentro de uma organização geralmente usados como projetos ou departamentos da mesma, proporcionando uma maneira eficiente de agrupar despesas e receitas, para uma melhor análise do negócio. 

A tela de centro de custo possibilita criar, atualizar e excluir centros de custo. Além de oferecer uma listagem de todos os centros de custo que podem ser organizados em pastas lhe permitindo uma hierarquia organizada.
 
![CentroDeCusto]

 **1.** Ao navegar entre as pastas até o centro de custo desejado é exibido na parte superior, que está em destaque na imagem, todo o caminho percorrido. Que permite facilmente se localizar e navegar entre as pastas, apenas clicando em uma delas.
 
 **2.** Permite voltar à raiz da árvore de centros de custo.

 **3.** Outra forma de navegar entre as pastas e centros de custo.


O gif abaixo facilita o entendimento dos itens descritos acima.

![NavegarEntrePastas]

## Reiniciar Base de Dados

Para o sistema disponíveis em servidores de homologação, estará disponível no menu principal essa opção, ao qual lhe permite reiniciar sua base de cadastros, para assim realizar novos testes. Dispõe duas opções para reinicialização da base de dados:

  *  Base de Homologação: Conterá dados fictícios que auxiliarão em treinamentos do sistema.
  * Base Limpa: Conterá apenas cadastros padrões, como por exemplo: Bancos, países e cidades.

![ReiniciarBase]

## Atualização da pesquisa em todos os cadastros

Pensando em melhorar a busca de registros, ao realizar uma pesquisa, o conteúdo pesquisado é destacado em cada um dos registros.

![highlight]

## Novas Funcionalidades na tela de Compromisso Financeiro

![CompromissoFinanceiro]

Nesta tela de listagem os valores dos compromissos financeiros do tipo despesa estão sendo destacados pela cor vermelha. Além disso podemos citar:

**1. Nova Despesa:** É possível incluir uma nova despesa, informando campos como: conta, centro de custo, data e valor da despesa.

![NovaDespesa]

**2. Nova Receita:** É disponibilizada a opção de inserir uma nova receita, ou seja, um compromisso financeiro que gera lucro para empresa. Informando os campos descritos na imagem.

![NovaReceita]

> Em nova despesa e nova receita os compromissos financeiros incluídos já são baixados, ou seja, já possuem uma baixa associada a eles.

[Menu]: Images/0.4.0/menu.png "Menu"
[CentroDeCusto]: Images/0.4.0/centroCusto.png "CentroDeCusto"
[NavegarEntrePastas]: Images/0.4.0/navegarEntrePastas.gif "NavegarEntrePastas"
[ReiniciarBase]: Images/0.4.0/reiniciarBase.png "ReiniciarBase"
[highlight]: Images/0.4.0/highlight.png "highlight"
[CompromissoFinanceiro]: Images/0.4.0/compromissoFinanceiro.png "CompromissoFinanceiro"
[NovaDespesa]: Images/0.4.0/novaDespesa.png "NovaDespesa"
[NovaReceita]: Images/0.4.0/novaReceita.png "NovaReceita"