# Documentação Markdown

Markdown é uma forma simples de formatar textos. A seguir vamos apresentar alguns comados que existem em markdown e seus exemplos. 

### Título

Para criar um título em markdown deve inserir o símbolo de cerquilha (`#`) ou a partir de três símbolos de igual (`=`) a baixo do texto.
  
![][exemploTitulo]

### Subtítulo

Para fazer subtítulo, insira o símbolo de 2 cerquilha (`##`) ou utilize mais de 2 hífen (`---`) abaixo do texto desejado.
  
![][exemploSubtitulo]

### Parágrafos
  
O que define um paragrafo é a divisão de bloco de texto com 1 linha em branco.
  
![][exemploParagrafo]

### Itálico
  
No parágrafo é possível colocar palavras ou frases em itálico, para isto a palavra ou a frase deve ficar entre símbolos de asterisco (`*`) ou entre os símbolos de sublinhado (`_`). 
  
![][exemploItalico]

### Negrito

No parágrafo é possível colocar palavras ou frases em negrito, para isto a palavra ou a farasse deve ficar entre símbolos de asterisco duplo (`**`) ou entre os símbolos de sublinhado duplo (`__`).
  
![][exemploNegrito]

 ### Lista
 
Outro recurso que markdown oferece é formatar listas. Para isso, utilize o asterisco (*) ou hífen (`-`) no início de cada linha, seguida da palavra ou frase desejada.
  
![][exemploLista]

Também é possível fazer listas ordenadas basta que cada elemento da lista inicie com o número seguido de ponto (`1.`) ou número seguido de parênteses para direita (`1)`). 
  
![][exemploListaOrdenada]

### Bloco de Citação
 
Você também pode formatar o texto para ser uma citação, para isto inicie a linha ou paragrafo com o símbolo de maior que (`>`). 
  
![][exemploBlocoCitacao]

### Links

Para formatar o texto em links, usamos a estrutura aseguir `[palavara que vai ser o link] (url do link)`. Existe outra forma de se utilizar que será mostrada no exemplo abaixo.
  
![][exemploLink]

### Imagens
 
MarkDown permite que você insira images utilizando essa estrura `![Aqui é o nome da imagem](Aqui é o link da imagem)`. Existe outra foram de fazer que é mostrada no exemplo abaixo. O nome da imagem não é obrigatorio, pode ser vazio. Outro ponto importatne é que apenas imagens de links públicos serão permitidos.

Exemplo de link de imagem permitido:

`![](http://imagem.png)`
 
Exemplo de link de imagem não permitido:

`![](C:\imagens\imagem.png)`
 
![][exemploImagem]

### Linha Horizontal

Para inserir uma linha horizontal deve-se colocar mais de 2 simbolos de hífen (`---`) três vezes ou mais de 2 símbolos de asterisco (`***`) com espaçamento de 1 linha.
  
![][exemploLinhaHorizontal]

### Código em Linha

Quando você precisar copiar alguma linha de código e colar no texto, mas você quer que ela fique com a mesma forma do original, então neste caso, você coloca a linha de código entre o símbolo de crase (**`` ` ``**). 
  
![][exemploCodigoLinha]

### Bloco de código

No caso de ser preciso copiar um bloco de código para o texto, manter o formato do bloco basta colocá-lo envolta do símbolo de crase (**`` ` ``**) três vezes.
  
![][exemploBlocoCodigo]


[exemploTitulo]: Imagens/exemploTitulo.png
[exemploSubtitulo]: Imagens/exemploSubtitulo.png
[exemploParagrafo]: Imagens/exemploParagrafo.png
[exemploNegrito]: Imagens/exemploNegrito.png
[exemploLink]: Imagens/exemploLink.png
[exemploListaOrdenada]: Imagens/exemploListaOrdenada.png
[exemploLista]: Imagens/exemploLista.png
[exemploItalico]: Imagens/exemploItalico.png
[exemploImagem]: Imagens/exemploImagem.png
[exemploBlocoCitacao]: Imagens/exemploBlocoCitacao.png
[exemploCodigoLinha]: Imagens/exemploCodigoLinha.png
[exemploBlocoCodigo]: Imagens/exemploBlocoCodigo.png
[exemploLinhaHorizontal]: Imagens/exemploLinhaHorizontal.png