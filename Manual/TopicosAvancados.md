# Tópicos Avançados

## Atalhos

Agora temos atalhos para o nosso sistema. Segue abaixo uma lista destes.

-  ALT + DIREITA  : Move  a janela com foco para a direita com metade da largura disponível
-  ALT + ESQUERDA : Move a janela com foco para a esquerda com metade da largura disponível
-  ALT + CIMA     : Maximiza a janela com foco
-  ALT + BAIXO    : Minimiza a janela com foco